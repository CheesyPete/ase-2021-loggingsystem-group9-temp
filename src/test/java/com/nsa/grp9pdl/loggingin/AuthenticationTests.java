package com.nsa.grp9pdl.loggingin;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationTests {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("When calling the /profile endpoint without authentication, we expect to get 302 redirect")
    void testUnauthenticatedProfileEndpoint() throws Exception {
        mockMvc.perform(get("/profile"))
                .andExpect(status().is(302));
    }

    @Test
    @DisplayName("When calling the /profile endpoint without proper authentication, we expect to get a 302 redirect as the user has not logged in")
    void testAuthenticatedWithoutProperAuthProfileEndPoint() throws Exception {
        mockMvc.perform(get("/profile"))
                .andExpect(status().is(302));
    }

    @Test
    @DisplayName("When calling the /profile endpoint with the correct login credentials using with mock user, we should get a 200 code and no errors presented")
    @WithMockUser(username="BarehamSA@cardiff.ac.uk", password = "password", roles = "USER")
    void testAuthenticatedWithProperAuthProfileEndPoint() throws Exception {
        mockMvc.perform(get("/profile"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("When calling the /profile endpoint with the incorrect login credentials, we should get redirected to /login?error")
    void testAuthenticateWithIncorrectCredentials() throws Exception {
        mockMvc.perform(formLogin().user("testEmail@cardiff.ac.uk").password("pass"))
                .andExpect(status().is(302));
    }

}
