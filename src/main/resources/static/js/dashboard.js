/*
 Template Name: Inspire - Bootstrap 4 Admin Dashboard
 Author: UIdeck
 Website: www.uideck.com
File: dashboard js
 */

!function ($) {
    "use strict";

    var Dashboard = function () {
    };

        //creates line chart
        Dashboard.prototype.createLineChart = function(element, data, xkey, ykeys, labels, lineColors) {
            Morris.Line({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            yLabelFormat: function(y){return y != Math.round(y)?'':y;},
            xLabelFormat: function(d) { return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear(); },
            labels: labels,
            hideHover: 'auto',
            gridLineColor: '#eef0f2',
            resize: true, //defaulted to true
            lineColors: lineColors
            });
        },

        //creates Donut chart
        Dashboard.prototype.createDonutChart = function (element, data, colors) {
            Morris.Donut({
                element: element,
                data: data,
                resize: true,
                colors: colors,
            });
        },

        Dashboard.prototype.init = function () {

            //create line chart
            this.createLineChart('morris-line-example', $linedata, 'y', $tagdata, $tagdata, ['#e22a6f', "#24d5d8", '#ab8ce4', '#28a745', '#1a2942', '#5c3548', '#52a39b', '#0e0712', '#540a73']);
    
            //creating donut charts for dashboard
            this.createDonutChart('morris-donut-example-a', $donutData1, ['#e22a6f', "#24d5d8", '#ab8ce4', '#28a745', '#1a2942', '#5c3548']);

            this.createDonutChart('morris-donut-example-b', $donutData2, ['#e22a6f', "#24d5d8", '#ab8ce4', '#28a745', '#1a2942', '#5c3548']);

            this.createDonutChart('morris-donut-example-c', $donutData3, ['#e22a6f', "#24d5d8", '#ab8ce4', '#28a745', '#1a2942', '#5c3548']);

        },
        //init
        $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.Dashboard.init();
    }(window.jQuery);