package com.nsa.grp9pdl.todo;

import lombok.Data;

@Data
public class SubmittedCheckboxes {
    private int id;
}
