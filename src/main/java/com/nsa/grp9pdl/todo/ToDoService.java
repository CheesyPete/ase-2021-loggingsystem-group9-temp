package com.nsa.grp9pdl.todo;

import com.nsa.grp9pdl.user_security.user_data.User;
import com.nsa.grp9pdl.user_security.user_data.UserRepositoryJPA;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ToDoService implements ToDoAuditor {
    private final ToDoRepositoryJPA todoRepo;
    private final UserRepositoryJPA userRepo;

    public ToDoService(ToDoRepositoryJPA tr, UserRepositoryJPA uR) {

        todoRepo = tr;
        userRepo = uR;

    }

    @Override
    public Integer createNewToDo(String Name, Integer userID) {
        ToDo newToDo = new ToDo();
        User newUser = userRepo.findUserById(userID);
        String passedName = Name;
        newToDo.setName(passedName);
        newToDo.setCompleted(false);
        newToDo.setUserID(newUser);
        todoRepo.save(newToDo);
        return newToDo.getTodoID();
    }

    public List<ToDo> findAllByUserIDAndCompleted(User UserID, boolean completed) {
        return todoRepo.findAllByUserIDAndCompleted(UserID, completed);
    }

    public void modifyToDo(SubmittedCheckboxes submittedCheckboxes) {
        ToDo currentToDo = todoRepo.findByTodoID(submittedCheckboxes.getId());
        currentToDo.setCompleted(true);
        todoRepo.save(currentToDo);
    }

}
