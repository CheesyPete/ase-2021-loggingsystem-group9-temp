package com.nsa.grp9pdl.todo;


import com.nsa.grp9pdl.user_security.user_data.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "todo")
public class ToDo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "todoid")
    private int todoID;
    private String name;
    private boolean completed;
    @OneToOne
    @JoinColumn(name = "UserID", referencedColumnName = "UserID")
    private User userID;

}
