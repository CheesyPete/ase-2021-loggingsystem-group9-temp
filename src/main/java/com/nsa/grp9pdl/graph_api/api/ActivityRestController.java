package com.nsa.grp9pdl.graph_api.api;

import java.util.ArrayList;
import java.util.List;

import com.nsa.grp9pdl.activity_management.tagging.TaggingService;
import com.nsa.grp9pdl.activity_management.tagging.Tags;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActivityRestController {
    @Autowired
    private TaggingService taggingService;

    @PostMapping(path = "/activity-tag-info", consumes = "application/json", produces = "application/json")
    public List<Tags> giveActivityTagInfo(@RequestBody ActivityNumber activity) {
        try {
            return taggingService.getTagsFromActivity(activity.getActivityID());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
