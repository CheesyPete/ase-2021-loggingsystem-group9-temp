package com.nsa.grp9pdl.graph_api.api;

import lombok.Data;

@Data
public class ActivityNumber {
    private Integer activityID;
}
