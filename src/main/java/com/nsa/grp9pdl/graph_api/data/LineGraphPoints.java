package com.nsa.grp9pdl.graph_api.data;

import java.util.Random;

import lombok.Data;

@Data
public class LineGraphPoints {
    private String date;
    private Integer a1Total;
    private Integer a2Total;

    public LineGraphPoints createData(String inDate) {
        Random random = new Random();

        date = inDate;
        a1Total = random.nextInt(11);
        a2Total = random.nextInt(11);

        return this;
    }
}
