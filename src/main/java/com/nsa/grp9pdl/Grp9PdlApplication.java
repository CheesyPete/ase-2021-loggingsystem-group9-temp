package com.nsa.grp9pdl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Grp9PdlApplication {

	public static void main(String[] args) {
		SpringApplication.run(Grp9PdlApplication.class, args);
	}

}
