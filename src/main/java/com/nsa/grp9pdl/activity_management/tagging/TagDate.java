package com.nsa.grp9pdl.activity_management.tagging;

import java.util.Date;

import lombok.Data;
import lombok.Setter;

@Data
public class TagDate {
    @Setter private Tags tag;
    @Setter private Date date;
}
