package com.nsa.grp9pdl.activity_management.tagging;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaggingRepositoryJPA extends JpaRepository<Tags, Integer> {
    Tags findByTagID(int tagID);

    List<Tags> findAll();
}
