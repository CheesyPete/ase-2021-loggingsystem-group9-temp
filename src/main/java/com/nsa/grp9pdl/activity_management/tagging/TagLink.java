package com.nsa.grp9pdl.activity_management.tagging;

import lombok.Data;

import javax.persistence.*;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;

@Data
@Entity
@Table(name = "taglink")
public class TagLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TaglinkID")
    private Integer taglinkid;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="activityID", referencedColumnName = "activityID")
    private Activity activityID;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="tagID", referencedColumnName = "tagID")
    private Tags tagID;

}
