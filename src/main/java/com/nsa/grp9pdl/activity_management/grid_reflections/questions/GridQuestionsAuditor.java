package com.nsa.grp9pdl.activity_management.grid_reflections.questions;

import java.util.List;

public interface GridQuestionsAuditor {
    GridQuestions getGridQuestions(Integer id);
    List<GridQuestions> findAll();
}
