package com.nsa.grp9pdl.activity_management.grid_reflections.questions;

import org.springframework.data.jpa.repository.JpaRepository;


public interface GridQuestionsRepo extends JpaRepository<GridQuestions, Integer> {
    GridQuestions getGridQuestionsByGridquestionID(Integer id);
}
