package com.nsa.grp9pdl.activity_management.grid_reflections.answers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;
import com.nsa.grp9pdl.activity_management.grid_reflections.questions.GridQuestions;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.CascadeType;
import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "gridanswers")
public class GridAnswers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gridanswerID")
    private int gridanswerID;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "gridquestionID", referencedColumnName = "gridquestionID")
    private GridQuestions gridquestionID;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "activityID", referencedColumnName = "activityID")
    private Activity activityID;

    @Column(name = "answer")
    private String answer;
}
