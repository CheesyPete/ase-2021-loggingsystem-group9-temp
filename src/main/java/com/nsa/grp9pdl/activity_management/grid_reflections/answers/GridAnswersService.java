package com.nsa.grp9pdl.activity_management.grid_reflections.answers;

import java.util.List;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;
import com.nsa.grp9pdl.activity_management.activity_data.ActivityService;
import com.nsa.grp9pdl.activity_management.grid_reflections.questions.GridQuestions;
import com.nsa.grp9pdl.activity_management.grid_reflections.questions.GridQuestionsService;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@Configuration
public class GridAnswersService implements GridAnswersAuditor {
    private final GridAnswersRepo gridAnswersRepo;
    private final GridQuestionsService gridQuestionsService;
    private final ActivityService activityService;

    public GridAnswersService(GridAnswersRepo gaR, ActivityService aS, GridQuestionsService gqS) {
        gridAnswersRepo = gaR;
        activityService = aS;
        gridQuestionsService = gqS;
    }

    private GridAnswers createNewGridAnswers(GridAnswersForm gridAnswersForm, Integer activityID, Integer gridQuestionID) {

        GridAnswers gridAnswers = new GridAnswers();
        Activity activity = activityService.getActivity(activityID);
        GridQuestions gridQuestion = gridQuestionsService.getGridQuestions(gridQuestionID);
        String answer = gridAnswersForm.getAnswer();
        gridAnswers.setActivityID(activity);
        gridAnswers.setGridquestionID(gridQuestion);
        gridAnswers.setAnswer(answer);

        return gridAnswers;
    }

    @Override
    public void saveGridAnswers(GridAnswersForm gridAnswersForm, Integer activity, Integer gridQuestion) {
        GridAnswers newKD = createNewGridAnswers(gridAnswersForm, activity, gridQuestion);
        gridAnswersRepo.save(newKD);
    }

    @Override
    public List<GridAnswers> findByActivityID(Integer id){
        Activity activity = activityService.getActivity(id);
        return gridAnswersRepo.findAllByActivityID(activity);
    }
}
