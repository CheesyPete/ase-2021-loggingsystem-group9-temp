package com.nsa.grp9pdl.activity_management.grid_reflections.answers;

import java.util.List;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GridAnswersRepo extends JpaRepository<GridAnswers, Integer> {
    List<GridAnswers> findAllByActivityID(Activity activity);
    
}
