package com.nsa.grp9pdl.activity_management.grid_reflections.data;

import lombok.Data;
import lombok.Setter;

@Data
public class GridQuestionAnswerPair {
    @Setter String question;
    @Setter String answer;
}
