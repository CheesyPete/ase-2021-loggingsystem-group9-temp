package com.nsa.grp9pdl.activity_management.grid_reflections.answers;

import java.util.List;

public interface GridAnswersAuditor {
    void saveGridAnswers(GridAnswersForm newReflection, Integer aID, Integer gID);
    List<GridAnswers> findByActivityID(Integer id);
}
