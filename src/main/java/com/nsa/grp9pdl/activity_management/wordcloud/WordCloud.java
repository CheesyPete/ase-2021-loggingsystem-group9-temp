package com.nsa.grp9pdl.activity_management.wordcloud;

import lombok.Data;

import javax.persistence.*;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;

import java.util.Map;

@Data
@Entity
@Table(name = "wordcloud")
public class WordCloud {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wordcloudid")
    private Integer wordCloudID;
    @OneToOne
    @JoinColumn(name = "activityID", referencedColumnName = "activityID")
    private Activity activityID;
    @Convert(converter = JpaConverterJson.class)
    @Column(name = "wordclouddata")
    private Map<String, Object> wordCloudData;
    
}
