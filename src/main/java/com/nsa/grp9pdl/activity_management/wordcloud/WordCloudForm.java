package com.nsa.grp9pdl.activity_management.wordcloud;

import lombok.Data;

@Data
public class WordCloudForm {
    private String wordclouddata;
}
