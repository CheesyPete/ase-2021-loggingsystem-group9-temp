package com.nsa.grp9pdl.activity_management.wordcloud;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WordCloudRepository extends JpaRepository<WordCloud, Integer> {
    WordCloud findWordCloudByActivityID(Activity activityID);
    WordCloud findWordCloudByWordCloudID(int Id);
}
