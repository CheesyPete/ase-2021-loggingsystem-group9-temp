package com.nsa.grp9pdl.activity_management.reflections;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReflectionsRepo extends JpaRepository<Reflections, Integer> {
    Reflections findByActivityID(Activity activity);

}
