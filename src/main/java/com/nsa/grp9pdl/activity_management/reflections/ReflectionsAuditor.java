package com.nsa.grp9pdl.activity_management.reflections;

import java.util.Optional;

public interface ReflectionsAuditor {
    void saveReflection(ReflectionForm newReflection, Integer id);
    void updateReflection(ReflectionForm newReflection, Integer id);

    Optional<Reflections> findReflectionsById(Integer id);
}
