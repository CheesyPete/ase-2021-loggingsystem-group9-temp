package com.nsa.grp9pdl.activity_management.activity_data;

import com.nsa.grp9pdl.user_security.user_data.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityRepositoryJPA extends JpaRepository<Activity, Integer> {
    List<Activity> findActivityByUserID (User userID);
    List<Activity> getActivitiesByUserID(User userID);


    Activity getActivityByActivityID(Integer id);

    List<Activity> findAllByUserID(User userID);
}
