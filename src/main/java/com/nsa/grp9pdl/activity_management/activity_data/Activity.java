package com.nsa.grp9pdl.activity_management.activity_data;

import com.nsa.grp9pdl.user_security.user_data.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Activities")
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int activityID;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date date;

    @Column(name="Title")
    private String title;
    @Column(name="Location")
    private String location;


    @OneToOne
    @JoinColumn(name = "UserID", referencedColumnName = "UserID")
    private User userID;

}
