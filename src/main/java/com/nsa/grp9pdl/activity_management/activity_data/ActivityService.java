package com.nsa.grp9pdl.activity_management.activity_data;

import com.nsa.grp9pdl.user_security.user_data.User;
import com.nsa.grp9pdl.user_security.user_data.UserRepositoryJPA;
import com.nsa.grp9pdl.activity_management.key_details.KeyDetails;
import com.nsa.grp9pdl.activity_management.key_details.KeyDetailsRepo;
import com.nsa.grp9pdl.activity_management.reflections.Reflections;
import com.nsa.grp9pdl.activity_management.reflections.ReflectionsRepo;
import com.nsa.grp9pdl.activity_management.tagging.TagLink;
import com.nsa.grp9pdl.activity_management.tagging.TagLinkRepositoryJPA;
import com.nsa.grp9pdl.user_security.security.CustomAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Stream;

@Component
public class ActivityService implements ActivityAuditor {
    private final ActivityRepositoryJPA activityRepo;
    private final UserRepositoryJPA userRepo;
    private final TagLinkRepositoryJPA taggingRepo;
    private final ReflectionsRepo reflectionsRepo;
    private final KeyDetailsRepo keyDetailsRepo;

    public ActivityService(ActivityRepositoryJPA aR, UserRepositoryJPA uR, TagLinkRepositoryJPA tR, ReflectionsRepo rR, KeyDetailsRepo kd) {
        activityRepo = aR;
        userRepo = uR;
        taggingRepo = tR;
        reflectionsRepo = rR;
        keyDetailsRepo = kd;
    }

    @Override
    public Integer createNewActivity(Date date, String title, String location) {

        Activity newActivity = new Activity();
        Date passedDate = date;
        String passedTitle = title;
        String passedLocation = location;


        User newUser = userRepo.findUserById(CustomAuthenticationSuccessHandler.currentUserDetails().getUserID());
        newActivity.setDate(passedDate);
        newActivity.setUserID(newUser);
        newActivity.setTitle(passedTitle);
        newActivity.setLocation(passedLocation);
        activityRepo.save(newActivity);

        return newActivity.getActivityID();
    }

    public Activity getActivity(Integer id) {
        return activityRepo.getActivityByActivityID(id);
    }
    public List<Activity> getAllActivities() {
        User newUser = userRepo.findUserById(CustomAuthenticationSuccessHandler.currentUserDetails().getUserID());
        return activityRepo.getActivitiesByUserID(newUser);
    }

    public List<Activity>  getActivitiesByUserID(User UserID){
        return activityRepo.findActivityByUserID(UserID);
    }

    public List<Integer> getActivityPercentage(List<Activity> activities){
        List<Integer> percentages = new ArrayList<>(activities.size());
        for(int i =0; i < activities.size(); i++) {
            Activity activity = activityRepo.getActivityByActivityID(i + 1);
            Reflections existsReflections = reflectionsRepo.findByActivityID(activity);
            KeyDetails existsKeyDetails = keyDetailsRepo.findByActivityID(activity);
            List<TagLink> existsTags = taggingRepo.findTagLinksByActivityID(activity);
            determinePercentageValue(percentages, existsReflections, existsKeyDetails, existsTags);
        }
        return percentages;
    }

    private void determinePercentageValue(List<Integer> percentages, Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        if (nothingEntered(existsReflections, existsKeyDetails, existsTags)) {
            percentages.add(0);
        } else if (allEntered(existsReflections, existsKeyDetails, existsTags)) {
            percentages.add(100);
        } else if (isJustKeyDetails(existsReflections, existsKeyDetails, existsTags) || isJustReflections(existsReflections, existsKeyDetails, existsTags) || isJustTags(existsReflections, existsKeyDetails, existsTags)) {
            percentages.add(33);
        } else if (allButTags(existsReflections, existsKeyDetails, existsTags) || allButReflections(existsReflections, existsKeyDetails, existsTags) || allButKeyDetails(existsReflections, existsKeyDetails, existsTags)) {
            percentages.add(66);
        }
    }

    ////// Methods to determine percentage values //////
    private boolean allButKeyDetails(Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        return areAllNotNull(existsReflections) && areAllNull(existsKeyDetails) && !existsTags.isEmpty();
    }

    private boolean allButReflections(Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        return areAllNotNull(existsKeyDetails) && areAllNull(existsReflections) && !existsTags.isEmpty();
    }

    private boolean allButTags(Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        return areAllNotNull(existsKeyDetails, existsReflections) && existsTags.isEmpty();
    }

    private boolean isJustTags(Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        return !existsTags.isEmpty() && areAllNull(existsKeyDetails, existsReflections);
    }

    private boolean isJustReflections(Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        return areAllNotNull(existsReflections) && areAllNull(existsKeyDetails) && existsTags.isEmpty();
    }

    private boolean isJustKeyDetails(Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        return areAllNotNull(existsKeyDetails) && areAllNull(existsReflections) && existsTags.isEmpty();
    }

    private boolean allEntered(Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        return areAllNotNull(existsKeyDetails, existsReflections) && !existsTags.isEmpty();
    }

    private boolean nothingEntered(Reflections existsReflections, KeyDetails existsKeyDetails, List<TagLink> existsTags) {
        return areAllNull(existsKeyDetails, existsReflections) && existsTags.isEmpty();
    }

    private boolean areAllNull(Object... objects){
        return Stream.of(objects).allMatch(Objects::isNull);
    }
    private boolean areAllNotNull(Object... objects){
        return Stream.of(objects).allMatch(Objects::nonNull);
    }
}
