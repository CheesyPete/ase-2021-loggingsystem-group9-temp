package com.nsa.grp9pdl.activity_management.activity_data;

import java.util.Date;

public interface ActivityAuditor {
    Integer createNewActivity(Date date, String title, String location);
}
