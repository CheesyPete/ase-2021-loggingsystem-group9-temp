package com.nsa.grp9pdl.activity_management.key_details;

import org.springframework.stereotype.Component;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;

@Component
public interface KeyDetailsAuditor {
    void saveKeyDetails(KeyDetailsForm keyDetailsForm, Integer aID);
    void updateKeyDetails(KeyDetailsForm keyDetails, Integer aID);
    KeyDetails findKeyDetailsByActivityID(Activity activity);
}
