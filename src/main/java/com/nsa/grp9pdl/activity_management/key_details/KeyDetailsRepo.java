package com.nsa.grp9pdl.activity_management.key_details;

import com.nsa.grp9pdl.activity_management.activity_data.Activity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KeyDetailsRepo extends JpaRepository<KeyDetails, Integer> {
    KeyDetails findByActivityID(Activity activity);
}
