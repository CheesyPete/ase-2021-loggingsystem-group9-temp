package com.nsa.grp9pdl.activity_management.quick_add;

import com.nsa.grp9pdl.activity_management.activity_data.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class QuickAddController {
    @Autowired
    private ActivityService activityService;

    @GetMapping("/new-activity/quick-add")
    public String showForm(Model model) {
        QuickAddForm quickAddForm = new QuickAddForm();
        model.addAttribute("qaForm", quickAddForm);
        return "quick-add";
    }

    private int saveAndEmail(QuickAddForm quickAddForm) {
        return activityService.createNewActivity(quickAddForm.getDate(), quickAddForm.getTitle(),
                quickAddForm.getLocation());
    }

    @PostMapping("new-activity/quick-add/dashboard")
    public String getForm(QuickAddForm quickAddForm, BindingResult bindings) {
        saveAndEmail(quickAddForm);

        return "redirect:/dashboard";
    }

    @PostMapping("new-activity/quick-add/wordcloud")
    public String getForm2(QuickAddForm quickAddForm, BindingResult bindings) {
        int id = saveAndEmail(quickAddForm);

        return "redirect:/wordcloud/" + id;
    }

    @PostMapping("new-activity/quick-add/grid-reflection")
    public String getForm3(QuickAddForm quickAddForm, BindingResult bindings) {
        int id = saveAndEmail(quickAddForm);

        return "redirect:/grid-reflection/" + id;
    }

    @PostMapping("new-activity/dimensions")
    public String getForm4(QuickAddForm quickAddForm, BindingResult bindings) {
        int id = saveAndEmail(quickAddForm);

        return "redirect:dimensions/" + id;
    }
}
