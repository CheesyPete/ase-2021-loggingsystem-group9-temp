package com.nsa.grp9pdl.user_security.sign_up;

import com.nsa.grp9pdl.user_security.user_data.User;
import com.nsa.grp9pdl.user_security.user_data.UserRepositoryJPA;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import javax.validation.Valid;

@Controller
public class SignUpController {
    static ConfigurableApplicationContext ctx;
    @Autowired
    private UserRepositoryJPA userRepositoryJPA;

    // No other logic is needed in this first annotation as all we need at this
    // point is the form to be presented
    @GetMapping("sign-up")
    public String signUpUser(Model model) {
        SignUpForm signUpForm = new SignUpForm();

        model.addAttribute("signUpForm", signUpForm);

        return "sign-up";
    }

    @PostMapping("account-create")
    public String newAccount(@Valid SignUpForm signUpForm, BindingResult bindings) {

        if (bindings.hasErrors()) {
            return "sign-up";

        } else {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            User user = new User(100000, signUpForm.getEmail(), signUpForm.getUsername(),
                    encoder.encode(signUpForm.getPassword()), signUpForm.getRole());

            userRepositoryJPA.save(user);
        }

        return "redirect:/login";

    }
}