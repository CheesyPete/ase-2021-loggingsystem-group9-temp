package com.nsa.grp9pdl.user_security.login;

import com.nsa.grp9pdl.user_security.user_data.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepoJPA extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String userName);
}
