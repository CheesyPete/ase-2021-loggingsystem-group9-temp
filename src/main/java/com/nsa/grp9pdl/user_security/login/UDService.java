package com.nsa.grp9pdl.user_security.login;

import com.nsa.grp9pdl.user_security.user_data.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UDService implements UserDetailsService {
    @Autowired
    private UserRepoJPA userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByEmail(username);
        user.orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));

        return user.map(MyUserDetails::new).get();
    }
}
